import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/User';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

    user: User
    userForm: FormGroup
    userId: string
        
    constructor(
        private dataService: DataService,
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
    
        this.userForm = this.formBuilder.group({
            name: ['', Validators.required],
			username: ['', Validators.required],
            email: ['', Validators.required],
            address: this.formBuilder.group({
                street: ['', Validators.required],
                suite: ['', Validators.required],
                city: ['', Validators.required],
                zipcode: ['', Validators.required],
              }),
            phone: ['', Validators.required],
            website: ['', Validators.required],
            company: this.formBuilder.group({
                name: ['', Validators.required],
                catchPhrase: ['', Validators.required],
                bs: ['', Validators.required],
              }),	
        })

        this.route.params.subscribe( params => {
            if(params.id){
                if(this.dataService.data) {
                    this.user = this.dataService.getUserById(params.id)
                    this.userForm.patchValue(this.user)
                    this.userId = params.id
                } else {
                    this.router.navigateByUrl('')
                }
            }
        });

        this.userForm.valueChanges.subscribe(user  => {
            this.user = user
            this.user.id = this.userId
        })
    }

}
