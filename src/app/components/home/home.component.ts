import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { DataService } from 'src/app/services/data.service';
import { User } from 'src/app/models/User';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

	dataSource: any
	displayedColumns: string[] = ['position', 'name', 'username', 'email'];
	selectedUser: User
	constructor(
		public dataService: DataService,
	) { }

	ngOnInit() {
		this.getData()
	}

	getData(){
		if(!this.dataService.data) {
			this.dataService.getAllUsers().subscribe(
			(data: User[]) => {
				this.dataSource = new MatTableDataSource(data)
				this.dataService.data = data
			})
		} else {
			this.dataSource = new MatTableDataSource(this.dataService.data)
		}

	}

	openProfile(user) {
		this.selectedUser = user
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

}
