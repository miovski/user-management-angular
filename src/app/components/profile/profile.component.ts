import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/User';
import { Router, ActivatedRoute, Data } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

	@Input() user: User
	edit: boolean = false
	

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private dataService: DataService
	) { }

	ngOnInit() {
		if( this.route.snapshot.routeConfig.path == 'user/:id') 
		this.edit = true
	}
	
	editUser(){
		this.router.navigateByUrl(`/user/${this.user.id}`);
	}

	updateUser(){
		this.dataService.updateUser(this.user)
		this.router.navigateByUrl('')
	}

	deleteUser(){
		this.dataService.deleteUser(this.user)
		this.router.navigateByUrl('')
  	}
}
