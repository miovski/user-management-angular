import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})

export class DataService {

    api = "https://jsonplaceholder.typicode.com/users"
    data: User[]

    constructor(
        private http: HttpClient
    ) { }

    getAllUsers(): Observable<User[]>{
        return this.http.get<User[]>(this.api);
    }

    getUserById(id:string) {
        return this.data.filter(x => x.id == id)[0];
    }

    updateUser(user: User) {
        let userIndex = this.data.findIndex(u => u.id == user.id);
        this.data[userIndex] = user
    }

    deleteUser(user: User) {
        this.data = this.data.filter(u => u.id !== user.id);
        this.updateIds()
    }

    updateIds() {
        this.data.forEach((element, index) => {
            element.id = (index + 1).toString()
        });
    }
}